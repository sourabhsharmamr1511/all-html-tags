// document.getElementById("demo").innerHTML = 6 + 6;

// document.write(5 + 6);

// window.alert(5 + 6);
// alert(5 + 6);
// console.log(5 + 6);

// let x, y, z;  // Statement 1
// x = 5;        // Statement 2
// y = 6;        // Statement 3
// z = x + y;    // Statement 4


// document.getElementById("demo").innerHTML= z;
// or 
// document.getElementById("demo").innerHTML = 
// "The value of z is " + z + ".";

// console.log(z);

/*var a = 100;
var b = 400;
console.log(a+b);*/

// function 
function myFunction() {
    document.getElementById("demo1").innerHTML = "Hello Dolly!";
    document.getElementById("demo2").innerHTML = "How are you?";
  }

  document.getElementById("demo").innerHTML = "John" + " "  + "Doe";

  // Change heading:
document.getElementById("myH").innerHTML = "JavaScript Comments";
// Change paragraph:
document.getElementById("myP").innerHTML = "My first paragraph.";

document.getElementById("myP").innerHTML = "My first paragraph.";


// let x = 16 + 4 + "Volvo";
// document.getElementById("demo").innerHTML = x;

let x = 5;
let y = 5;
let z = 6;

document.getElementById("demo").innerHTML =
(x == y) + "<br>" + (x == z);

// JavaScript Arrays
// JavaScript Arrays are written with square brackets 
const cars = ["Saab","Volvo","BMW"];

document.getElementById("demo").innerHTML = cars[0];

// JavaScript objects are written with curly braces {}
const person = {firstName:"John", lastName:"Doe", age:50, eyeColor:"blue"};
document.getElementById("demo").innerHTML =
person.firstName + " is " + person.age + " years old.";

// JavaScript typeof
document.getElementById("demo").innerHTML = 
typeof "" + "<br>" +
typeof "John" + "<br>" + 
typeof "John Doe";

// function 1

// function myFunction(p1, p2) {
//   return p1 * p2;
// }
// document.getElementById("demo").innerHTML = myFunction(4, 3);

// Undefined
// let car;
// document.getElementById("demo").innerHTML =
// car + "<br>" + typeof car;

// function 2

// var x = myFunction(4, 3);
// document.getElementById("demo").innerHTML = x;

// function myFunction(a, b) {
//   return a * b;

  // function 3

  // function toCelsius(f) {
  //   return (5/9) * (f-32);
  // }
  // document.getElementById("demo").innerHTML = toCelsius(77);

  // Functions Used as Variable Values

//   document.getElementById("demo").innerHTML =
// "The temperature is " + toCelsius(77) + " Celsius";

// function toCelsius(fahrenheit) {
//   return (5/9) * (fahrenheit-32);
// }

                          // Local Variables
                          // Variables declared within a JavaScript function, become LOCAL to the function.

                          // Local variables can only be accessed from within the function.

                          myFunction();

                              function myFunction() {
                                let carName = "Volvo";
                                document.getElementById("demo1").innerHTML =
                                typeof carName + " " + carName;
                              }

                              document.getElementById("demo2").innerHTML =
                              typeof carName;

                              // Create an object:

const car = {type:"Fiat", model:"500", color:"white"};

// Display some data from the object:

// document.getElementById("demo3").innerHTML = "The car type is " + car["type"];
// or 
document.getElementById("demo3").innerHTML = "The car type is " + car.type;

// this Keyword


// Create an object:
// const person = {
//   firstName: "John",
//   lastName: "Doe",
//   id: 5566,
//   fullName: function() {
//     return this.firstName + " " + this.lastName;
//   }
// };

// // Display data from the object:
// document.getElementById("demo").innerHTML = person.fullName();


function displayDate() {
  document.getElementById("demo").innerHTML = Date();
}
  // JavaScript String Properties
  let text = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
document.getElementById("len").innerHTML = text.length;

// string
// let answer1 = "It's alright";
// let answer2 = "He is called 'Johnny'";
// let answer3 = 'He is called "Johnny"'; 

// document.getElementById("str").innerHTML =
// answer1 + "<br>" + answer2 + "<br>" + answer3; 

// string length 

// let text = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
// document.getElementById("str").innerHTML = text.length;

// error 

let text2 = "We are the so-called \"Vikings\" from the north.";
document.getElementById("str").innerHTML = text2; 

let text3 = "The character \\ is called backslash.";
document.getElementById("demo").innerHTML = text3; 

// document.getElementById("str2").innerHTML = "Hello \
// Dolly!";
// or 
// document.getElementById("demo").innerHTML = "Hello " +
// "Dolly!";

// x is a string
let m = "John";

// y is an object
// let n = new String("John");

// document.getElementById("str3").innerHTML =
// typeof m + "<br>" + typeof n;

// document.getElementById("demo").innerHTML = (x==y);

let str = "Apple, Banana, Kiwi";
// document.getElementById("str3").innerHTML = str.slice(7,13); 
// document.getElementById("demo").innerHTML = str.slice(-12,-6);
// document.getElementById("demo").innerHTML = str.slice(7);
// document.getElementById("str3").innerHTML = str.substring(7,13);

document.getElementById("str3").innerHTML = str.substr(7,5);
document.getElementById("demo").innerHTML = str.substr(7);

// replace error
// function myFunction() {
//   let text = document.getElementById("demos").innerHTML;
//   document.getElementById("demos").innerHTML =
//   text.replace("Microsoft","W3Schools");
// }
// By default, the replace() method is case sensitive. Writing MICROSOFT (with upper-case) will not work:
// To replace case insensitive, use a regular expression with an /i flag (insensitive):

// function myFunction() {
//   let text = document.getElementById("demos").innerHTML; 
//   document.getElementById("demos").innerHTML =
//   text.replace(/MICROSOFT/i,"W3Schools");
// }

// To replace all matches, use a regular expression with a /g flag (global match):
// function myFunction() {
//   let text = document.getElementById("demos").innerHTML; 
//   document.getElementById("demos").innerHTML =
//   text.replace(/Microsoft/g,"W3Schools");
// }

// toUpperCase
// function myFunction() {
//   let text = document.getElementById("demos").innerHTML; 
//   document.getElementById("demos").innerHTML =
//   text.toUpperCase();
console.log(text.toUpperCase());
// }

let text1 = "Hello";
let text20 = "World!";
let text30 = text1.concat(" ",text20);
// document.getElementById("demo10").innerHTML = text3;
console.log(text30)

// trim method 
let text11 = "     Hello World!     ";
let text12 = text11.trim();

document.getElementById("demo6").innerHTML =
"Length text11=" + text11.length + "<br>Length12 text12=" + text12.length;

// String methods: padStart and padEnd to support padding at the beginning and at the end of a string.
let textp = "5";
document.getElementById("demo7").innerHTML = textp.padStart(4,0);
document.getElementById("demo7").innerHTML = textp.padEnd(4,0);

// Extracting String Characters
var texts = "HELLO WORLD";
document.getElementById("demo").innerHTML = texts.charAt(2);
document.getElementById("demo").innerHTML = text.charCodeAt(0);
document.getElementById("demo").innerHTML = texts[0];

// JavaScript String split()
let texta = "a,b,c,d,e,f";
const myArray = texta.split(",");
document.getElementById("demoas").innerHTML = myArray[0];

// example2
// let textab = "Hello";
// const myArrs = textab.split("");

// textab = "";
// for (let i = 0; i < myArrs.length; i++) {
//   textab += myArrs[i] + "<br>"
// }
// document.getElementById("demoas").innerHTML = textab;

// JavaScript Search Methods
let strp = "Please locate where 'locate' occurs!";
document.getElementById("demo11").innerHTML = strp.indexOf("locate");
document.getElementById("demo11").innerHTML = strp.lastIndexOf("locate");

// Both methods accept a second parameter as the starting position for the search:
document.getElementById("demo").innerHTML = str.indexOf("locate",15);
document.getElementById("demo").innerHTML = str.lastIndexOf("locate", 15);

// document.getElementById("demo").innerHTML = str.search("locate");

// // JavaScript String match()
// document.getElementById("demo11").innerHTML = text.match(/ain/g);

// /* <p>Perform a global, case-insensitive search for "ain":</p> */
// document.getElementById("demo").innerHTML = text.match(/ain/gi);


// // The includes() method returns true if a string contains a specified value.
// let text = "Hello world, welcome to the universe.";
// document.getElementById("demo").innerHTML = text.includes("world");

// // The startsWith() method returns true if a string begins with a specified value, otherwise false:
// document.getElementById("demo").innerHTML = text.startsWith("Hello");

// // The endsWith() method returns true if a string ends with a specified value, otherwise false:
// document.getElementById("demo").innerHTML = text.endsWith("Doe");
// // >Check in the 11 first characters of a string ends with "world"
// document.getElementById("demo").innerHTML = text.endsWith("world", 11);

// Template literals allow variables in strings:
let firstName = "John";
let lastName = "Doe";

let lit = `Welcome ${firstName}, ${lastName}!`;

document.getElementById("demo8").innerHTML = lit

// The toString() method converts a number to a string.
// let a = 123;
// document.getElementById("demo").innerHTML =
//   a.toString() + "<br>" +
//    (123).toString() + "<br>" +
//    (100 + 23).toString();


  //  The toExponential() method returns a string, with the number rounded and written using exponential notation
   let a = 9.656;
// document.getElementById("demo").innerHTML =
//   x.toExponential() + "<br>" + 
//   x.toExponential(2) + "<br>" + 
//   x.toExponential(4) + "<br>" + 
//   x.toExponential(6);

  // document.getElementById("demo").innerHTML =
  // x.toFixed(0) + "<br>" +
  // x.toFixed(2) + "<br>" +
  // x.toFixed(4) + "<br>" +
  // x.toFixed(6);

  document.getElementById("demo").innerHTML = 
  x.toPrecision() + "<br>" +
  x.toPrecision(2) + "<br>" +
  x.toPrecision(4) + "<br>" +
  x.toPrecision(6);

  // Number() can be used to convert JavaScript variables to numbers:
  // document.getElementById("demo").innerHTML = 
  // Number(true) + "<br>" +
  // Number(false) + "<br>" +
  // Number("10") + "<br>" + 
  // Number("  10") + "<br>" +
  // Number("10  ") + "<br>" +
  // Number(" 10  ") + "<br>" +
  // Number("10.33") + "<br>" + 
  // Number("10,33") + "<br>" +
  // Number("10 33") + "<br>" +
  // Number("John");

  // parseInt() parses a string and returns a whole number. Spaces are allowed. Only the first number is returned:
  // document.getElementById("demo").innerHTML = 
  // parseInt("-10") + "<br>" +
  // parseInt("-10.33") + "<br>" +
  // parseInt("10") + "<br>" +
  // parseInt("10.33") + "<br>" +
  // parseInt("10 6") + "<br>" +  
  // parseInt("10 years") + "<br>" + 
  // parseFloat("10.33") + "<br>" +
  // parseFloat("10 6") + "<br>" +  
  // parseFloat("10 years") + "<br>" + 
  // parseInt("years 10");
  
  // Array 
//   const cars1 = [
//     "Saab",
//     "Volvo",
//     "BMW"
//   ];
//   document.getElementById("demo").innerHTML = cars1;
//   // or 
//   const cars = [];
// cars[0]= "Saab";
// cars[1]= "Volvo";
// cars[2]= "BMW";
// // or 
// const cars = new Array("Saab", "Volvo", "BMW");
// document.getElementById("demo").innerHTML = cars;

// example1
const  cars2 = ["Saab", "Volvo", "BMW"];
 cars2[0] = "Opel";
document.getElementById("demo").innerHTML =  cars2;
document.getElementById("demo").innerHTML =  cars2.length;

// or 

const person1 = {firstName:"John", lastName:"Doe", age:46};
document.getElementById("demo").innerHTML = person1.firstName;

// or 

// const fruits = ["Banana", "Orange", "Apple", "Mango"];
// document.getElementById("demo").innerHTML = fruits[0];
// document.getElementById("demo").innerHTML = fruits[fruits.length-1];

// example 2
// const fruits1 = ["Banana", "Orange", "Apple", "Mango"];
// let fLen = fruits1.length;

// let text100 = "<ul>";
// for (let i = 0; i < fLen; i++) {
//   text100 += "<li>" + fruits1[i] + "</li>";
// }
// text100 += "</ul>";
// document.getElementById("demo").innerHTML = text100;

// Array.forEach() Example 

// const fruits = ["Banana", "Orange", "Apple", "Mango"];

// let  text103 = "<ul>";
// fruits.forEach(myFunction);
//  text103 += "</ul>";
// document.getElementById("demo").innerHTML =  text103;

// function myFunction(value) {
//    text103 += "<li>" + value + "</li>";
// }

// push element in array error
// const fruits = ["Banana", "Orange", "Apple"];
// document.getElementById("demo").innerHTML = fruits;

// function myFunction() {
//   fruits.push("Lemon");
//   document.getElementById("demo").innerHTML = fruits;
// }
// or 
// const fruits = ["Banana", "Orange", "Apple"];
// document.getElementById("demo").innerHTML = fruits;

// function myFunction() {
//   fruits[fruits.length] = "Lemon";
//   document.getElementById("demo").innerHTML = fruits;
// }

// const fruits = ["Banana", "Orange", "Apple"];
// fruits[6] = "Lemon";

// let fLen = fruits.length;
// let text104 = "";
// for (i = 0; i < fLen; i++) {
//   text104 += fruits[i] + "<br>";
// }

// document.getElementById("demo").innerHTML = text104;

// const person2 = [];
// person2[0] = "John";
// person2[1] = "Doe";
// person2[2] = 46; 
// document.getElementById("demo").innerHTML =
// person2[0] + " " + person2.length;

// // or 
// // var points = [40];
// // document.getElementById("demo").innerHTML = points;

// const fruits = ["Banana", "Orange", "Apple"];
// document.getElementById("demo").innerHTML = Array.isArray(fruits);
// document.getElementById("demo").innerHTML = fruits instanceof Array;
// document.getElementById("demo").innerHTML = Array.isArray(fruits);
// document.getElementById("demo").innerHTML = fruits instanceof Array;

// // Converting Arrays to Strings
// const fruits = ["Banana", "Orange", "Apple", "Mango"];
// document.getElementById("demo").innerHTML = fruits.toString();

// // It behaves just like toString(), but in addition you can specify the separator:
// document.getElementById("demo").innerHTML = fruits.join(" * ");

// // push and pop 
// const fruits = ["Banana", "Orange", "Apple", "Mango"];
// document.getElementById("demo1").innerHTML = fruits;
// fruits.pop();
// document.getElementById("demo2").innerHTML = fruits;
// // or 
// document.getElementById("demo1").innerHTML = fruits;
// fruits.push("Kiwi");
// document.getElementById("demo2").innerHTML = fruits;
// document.getElementById("demo1").innerHTML = fruits.push("Kiwi");

// JavaScript Array shift()
// The shift() method removes the first array element and "shifts" all other elements to a lower index.
// fruits.shift();
// document.getElementById("demo2").innerHTML = fruits;

// // The unshift() method adds a new element to an array (at the beginning), and "unshifts" older elements
// fruits.unshift("Lemon");
// document.getElementById("demo2").innerHTML = fruits;

              // JavaScript Array length
// The length property provides an easy way to append a new element to an array
// fruits[fruits.length] = "Kiwi";
// document.getElementById("demo2").innerHTML = fruits;

              // // JavaScript Array delete()
// delete fruits[0];
// document.getElementById("demo2").innerHTML =fruits


              // // Merging (Concatenating) Arrays
// // The concat() method creates a new array by merging (concatenating) existing arrays
// const myGirls = ["Cecilie", "Lone"];
// const myBoys = ["Emil", "Tobias", "Linus"];
// const myChildren = myGirls.concat(myBoys);
// const myChildren = myGirls.concat(sharma);
// // or 
// // or 

// document.getElementById("demo").innerHTML = myChildren;

// const array1 = ["Cecilie", "Lone"];
// const array2 = ["Emil", "Tobias", "Linus"];
// const array3 = ["Robin", "Morgan"];

// const myChildren = array1.concat(array2, array3); 

// document.getElementById("demo").innerHTML = myChildren;

// const fruits = ["Banana", "Orange", "Apple", "Mango"];
// document.getElementById("demo1").innerHTML = fruits;

                  // JavaScript Array splice()
// The splice() method can be used to add new items to an array:

const fruitsp = ["Banana", "Orange", "Apple", "Mango"];
document.getElementById("demo1").innerHTML = fruitsp;
fruitsp.splice(2, 0, "Lemon", "Kiwi");

document.getElementById("demo2").innerHTML = fruitsp;
// The first parameter (2) defines the position where new elements should be added (spliced in).

// The second parameter (0) defines how many elements should be removed.

const citrus = fruitsp.slice(1,3);
document.getElementById("demo").innerHTML = fruitsp + "<br><br>" + citrus;

                    // Array sort 
                    const fruits = ["Banana", "Orange", "Apple", "Mango"];
                    // First sort the array
   fruits.sort();

// Then reverse it:
fruits.reverse();
document.getElementById("asd").innerHTML = fruits;

// for number sorting
const points = [40, 100, 1, 5, 25, 10];
// Sorting ascending
points.sort(function(a, b){return a - b});
// Sorting descending
// points.sort(function(a, b){return b - a});
// points.sort(function(a, b){return 0.5 - Math.random})
document.getElementById("asd").innerHTML = points;
// document.getElementById("asd").innerHTML = points[0];

// Using Math.max() on an Array
document.getElementById("demo").innerHTML = myArrayMax(points);

function myArrayMax(arr) {
  return Math.max.apply(null, arr);
  // return Math.min.apply(null, arr);
}

// example2 
// const carsa = [
//   {type:"Volvo", year:2016},
//   {type:"Saab", year:2001},
//   {type:"BMW", year:2010}
// ];

// displaycarsa();

// carsa.sort(function(a, b){return a.year - b.year});
// displaycarsa();

// function displaycarsa() {
//   document.getElementById("demo").innerHTML =
//   carsa[0].type + " " + carsa[0].year + "<br>" +
//   carsa[1].type + " " + carsa[1].year + "<br>" +
//   carsa[2].type + " " + carsa[2].year;
// }

                          // Array Iteration error
//                           const numbers = [45, 4, 9, 16, 25];

// let txt = "";
// numbers.forEach(myFunction);
// document.getElementById("ai").innerHTML = txt;

// function myFunction(value, index, array) {
//   txt += value + "<br>";
// }
// map example 
const numbers1 = [45, 4, 9, 16, 25];
const numbers2 = numbers1.map(myFunction);

document.getElementById("ai").innerHTML = numbers2;

function myFunction(value, index, array) {
  return value * 2;
}

                // JavaScript Array filter()
                const numbers = [45, 4, 9, 16, 25];
const over18 = numbers.filter(myFunction);
// const over18 = numbers.every(myFunction);
// const over18 = numbers.some(myFunction);
// const over18 = numbers.find(myFunction);
// const over18 = numbers.findIndex(myFunction)



let position = numbers.indexOf("Apple") + 1;
// document.getElementById("ai").innerHTML = position;

document.getElementById("ai").innerHTML = over18;

function myFunction(value, index, array) {
  return value > 18;
}
                                       // Array reduce() works from left-to-right in the array
// const numbers = [45, 4, 9, 16, 25];
// let sum = numbers.reduce(myFunction);

// document.getElementById("ai").innerHTML = "The sum is " + sum;

// function myFunction(total, value) {
//   return total + value;
// }

                                  // Date Objects
                                  // let myDate = new Date();
                                  const d = new Date();
                                  // const d = new Date("2015-03-25");
                                  // const d = new Date(2018, 11, 24, 10, 33, 30, 0);
                                  // const d = new Date(2018, 11, 24, 10);
                                  document.getElementById("dt").innerHTML = d;
// console.log(myDate.getTime());
// console.log(myDate.getFullYear());
// console.log(myDate.getDay());
// console.log(myDate.getMinutes());
// console.log(myDate.getHours());
                                   
                                      //  Math 

document.getElementById("demo").innerHTML = Math.round(4.4);

// Math.ceil(x) returns the value of x rounded up to its nearest integer
document.getElementById("demo").innerHTML = Math.ceil(4.4);

// Math.floor(x) returns the value of x rounded down to its nearest integer
document.getElementById("demo").innerHTML = Math.floor(4.7);

// Math.trunc(x) returns the integer part 
document.getElementById("demo").innerHTML = Math.trunc(4.7);

// Math.pow(x,y) returns the value of x to the power of y:
document.getElementById("demo").innerHTML = Math.pow(8,2);
document.getElementById("demo").innerHTML = Math.sqrt(64);

                                      // Random
                                      // document.getElementById("demo").innerHTML = Math.random();
   document.getElementById("demo").innerHTML =
Math.floor(Math.random() * 100) + 1;
                                      // Conditional 
const time = new Date().getHours();
let greeting;
if (time < 10) {
  greeting = "Good morning";
} else if (time < 20) {
  greeting = "Good day";
} else {
  greeting = "Good evening";
}
document.getElementById("demo").innerHTML = greeting;
                                        
                                              // Switch 
      
                                              let day;
                                              switch (new Date().getDay()) {
                                                case 0:
                                                  day = "Sunday";
                                                  break;
                                                case 1:
                                                  day = "Monday";
                                                  break;
                                                case 2:
                                                  day = "Tuesday";
                                                  break;
                                                case 3:
                                                  day = "Wednesday";
                                                  break;
                                                case 4:
                                                  day = "Thursday";
                                                  break;
                                                case 5:
                                                  day = "Friday";
                                                  break;
                                                case  6:
                                                  day = "Saturday";
                                              }
                                              document.getElementById("demo").innerHTML = "Today is " + day;

                                              // switch example 2 
// let text;
// switch (new Date().getDay()) {
//   case 4:
//   case 5:
//     text = "Soon it is Weekend";
//     break;
//   case 0:
//   case 6:
//     text = "It is Weekend";
//     break;
//   default:
//     text = "Looking forward to the Weekend";
// }
// document.getElementById("demo").innerHTML = text;

                                                  // for loop 

const carsarr = ["BMW", "Volvo", "Saab", "Ford", "Fiat", "Audi"];

let textf = "";
for (let i = 0; i < carsarr.length; i++) {
  textf += carsarr[i] + "<br>";
}

document.getElementById("lfor").innerHTML = textf;

// or 

// const cars = ["BMW", "Volvo", "Saab", "Ford"];

// let i = 0;
// let text = "";
// for (;cars[i];) {
//   text += cars[i] + "<br>";
//   i++;
// }

// document.getElementById("demo").innerHTML = text;

                            // loop for in 
                            const personf = {fname:"John", lname:"Doe", age:25}; 

let txt = "";
for (let x in personf) {
  txt += personf[x] + " ";
}

document.getElementById("lfori").innerHTML = txt;

                            // Array.forEach()

//                             const numbersfe = [45, 4, 9, 16, 25];

// let txtfe = "";
// numbersfe.forEach(myFunction);
// document.getElementById("lfore").innerHTML = txtfe;

// function myFunction(value, index, array) {
//   txtfe += value + "<br>";
// }

                                //  loop For Of 

                                const carsof = ["BMW", "Volvo", "Mini"];

let txtof = "";
for (let x of carsof) {
  txtof += x + "<br>";
}

document.getElementById("lforo").innerHTML = txtof;

                              // loop while 

                              let textwhile = "";
let i = 0;
while (i < 10) {
  textwhile += "<br>The number is " + i;
  i++;
}
document.getElementById("lwhile").innerHTML = textwhile;

// or 

// const cars = ["BMW", "Volvo", "Saab", "Ford"];

// let i = 0;
// let text = "";
// while (cars[i]) {
//   text += cars[i] + "<br>";
//   i++;
// }

// document.getElementById("demo").innerHTML = text;
 
    // Do while 
//     let text = ""
// let i = 0;

// do {
//   text += "<br>The number is " + i;
//   i++;
// }
// while (i < 10);  

// document.getElementById("demo").innerHTML = text;

                                    // Break, continue 

                                    let textb = "";
for (let i = 0; i < 10; i++) {
  // if (i === 3) { break; }
  if (i === 3) { continue; }
  textb += "The number is " + i + "<br>";
}

document.getElementById("brk").innerHTML = textb;

                                        // Iterables

 // Create a String
const namei = "W3Schools";

// List all Elements
let textitb = ""
for (const x of namei) {
  textitb += x + "<br>";
}

document.getElementById("itb").innerHTML = textitb;

// example2 

// const fruits = new Map([
//   ["apples", 500],
//   ["bananas", 300],
//   ["oranges", 200]
// ]);

// // List all entries
// let text = "";
// for (const x of fruits) {
//   text += x + "<br>";
// }

// document.getElementById("demo").innerHTML = text;

                                                        //  Sets
                                                        // Create a Set
const letters = new Set();

// Add values to the Set
letters.add("a");
letters.add("b");
letters.add("c");
letters.add("c");
letters.add("c");
letters.add("c");
letters.add("c");
letters.add("c");

// Display set.size
document.getElementById("set").innerHTML = letters.size; 

// set example 2

// // Create a Set
// const letters = new Set(["a","b","c"]);

// // List all Elements
// let textset = "";
// letters.forEach (function(value) {
//   textset += value + "<br>";
// })

// document.getElementById("set").innerHTML = textset;

                                              // Map
                                              
                                              // const fruits = new Map([
                                              //   ["apples", 500],
                                              //   ["bananas", 300],
                                              //   ["oranges", 200]
                                              // ]);
                                              
                                              // document.getElementById("demo").innerHTML = fruits.get("apples");

                                              // Map example  2

                                              // Create a Map
// const fruitsm = new Map();

// // Set Map Values
// fruitsm.set("apples", 500);
// fruitsm.set("bananas", 300);
// fruitsm.set("oranges", 200);

// // Delete an Element
// // fruits.delete("apples");

// document.getElementById("map").innerHTML = fruitsm.get("apples");
// document.getElementById("map").innerHTML = fruitsm.size;

// The has() method returns true if a key exists in a Map
// document.getElementById("demo").innerHTML = fruits.has("apples");

// example 3

// Create a Map
const fruitsmp = new Map([
  ["apples", 500],
  ["bananas", 300],
  ["oranges", 200]
]);

let textm = "";
fruitsmp.forEach (function(value, key) {
  textm += key + ' = ' + value + "<br>"
})

document.getElementById("map").innerHTML = textm;

// or 

// Create a Map
// const fruits = new Map([
//   ["apples", 500],
//   ["bananas", 300],
//   ["oranges", 200]
// ]);

// let text = "";
// for (const x of fruits.entries()) {
//   text += x + "<br>";
// }

// document.getElementById("demo").innerHTML = text;

                              // search method 

                              let textsr = "Visit W3Schools!"; 
let n = textsr.search("W3Schools");
// let n = textsr.replace("W3Schools", "sharma");
document.getElementById("sr").innerHTML = n;

